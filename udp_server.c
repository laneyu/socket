#include <stdio.h>
#include <stdlib.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <fcntl.h>

static char *get_address(int sockfd);
static void print_client_info(struct sockaddr_in * p);
static void print_socket_info(int sockfd);
int SetSocketBlockingEnabled(int sockfd, int blocking);

int main(void)
{
	int sockfd;
	struct sockaddr_in server_addr, client_addr;
	const unsigned short server_port = 50000;
	
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if ( sockfd < 0 )
	{
		printf("socket error in %s...\n", __FUNCTION__);
		return -1;
	}
	
	bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port   = htons(server_port);
//	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
//	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	server_addr.sin_addr.s_addr = inet_addr("192.168.18.128");
	if ( bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)))
	{
		printf("bind error in %s...\n", __FUNCTION__);
		return -1;
	}

	printf("server ip: %s\n", get_address(sockfd));
	print_socket_info(sockfd);
		
	int n;
	socklen_t len = sizeof(client_addr);
//	socklen_t len;
	char msg[256];
	SetSocketBlockingEnabled(sockfd, 0);
	for (;;)
	{
		n = recvfrom(sockfd, msg, 256, 0, (struct sockaddr *)&client_addr, &len);

		printf("server ip: %s\n", get_address(sockfd));
		print_client_info(&client_addr);
		printf("msg: %s\n", msg);
		char reply[256] = "server:";
		
		sendto(sockfd, reply, strlen(reply), 0, (struct sockaddr *)&client_addr, sizeof(client_addr));
		sendto(sockfd, msg, n, 0, (struct sockaddr *)&client_addr, sizeof(client_addr));
		
		sleep(1);
	}

	return 0;
}

static char *get_address(int sockfd)
{
	static char ip[128];
	struct sockaddr_in sock_addr;
	socklen_t len = sizeof(sock_addr);
	
	if ( getsockname(sockfd, (struct sockaddr *)&sock_addr, &len) < 0 )
	{
		printf("getsockname error in %s...\n", __FUNCTION__);
		
		return NULL;
	}
	
	strcpy(ip, inet_ntoa(sock_addr.sin_addr));
	
	return ip;
}

static void print_client_info(struct sockaddr_in * p)
{
	printf("client ip: %s\n", inet_ntoa(p->sin_addr));
	
	return ;
}

static void print_socket_info(int sockfd)
{
	int timeout = 0;
	socklen_t optlen = sizeof(timeout);
	
	getsockopt( sockfd, SOL_SOCKET, SO_SNDTIMEO, &timeout, &optlen );
	printf("sockfd sendtimeout: 		%d\n", timeout);
	getsockopt( sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, &optlen );
	printf("sockfd sendtimeout: 		%d\n", timeout);
	
	int send_buf = 0;
	int recv_buf = 0;
	optlen = sizeof(send_buf);
	getsockopt( sockfd, SOL_SOCKET, SO_SNDBUF, &send_buf, &optlen );
	printf("sockfd send buffer size:	%d\n", send_buf);
	getsockopt( sockfd, SOL_SOCKET, SO_RCVBUF, &recv_buf, &optlen );
	printf("sockfd recv buffer size:	%d\n", recv_buf);
	
	getsockopt( sockfd, SOL_SOCKET, SO_SNDBUF, &send_buf, &optlen );
	printf("sockfd send lowest buffer size:	%d\n", send_buf);
	getsockopt( sockfd, SOL_SOCKET, SO_RCVBUF, &recv_buf, &optlen );
	printf("sockfd send lowest buffer size:	%d\n", recv_buf);
}

// blocking 1 for blocking, 0 for non-blocking
int SetSocketBlockingEnabled(int sockfd, int blocking)
{
	int flags;
	
	if ( sockfd < 0 )
	{
		return -1;
	}
	
	flags = fcntl(sockfd, F_GETFL, 0);
	if ( flags < 0 )
	{
		return -1;
	}
	flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
	
	if ( fcntl(sockfd, F_SETFL, flags) < 0 )
	{
		return -1;
	}
	
	return 0;
}

//int opt = 1;
//ioctl(fd, FIONBIO, &opt);













