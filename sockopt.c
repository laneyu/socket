#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/tcp.h>
#include <netinet/in.h>

int main()
{
	//TCP socket disable Nagle 
	int sock, flag = 0, ret;
	socklen_t optlen = sizeof(flag);
	/* Create new stream socket */
	sock = socket( AF_INET, SOCK_STREAM, 0 );
	/* Disable the Nagle (TCP No Delay) algorithm */
	flag = 0;		/* 0--enable, 1--disable */
	ret = getsockopt( sock, IPPROTO_TCP, TCP_NODELAY, &flag, &optlen );
	if (ret == -1) {
	  printf("Couldn't getsockopt(TCP_NODELAY)\n");
	  exit(-1);
	}
	printf("%s\n", flag == 0 ? "enable" : "disable");
	
#if 0
	int sock, flag, ret;
	/* Create new stream socket */
	sock = socket( AF_INET, SOCK_STREAM, 0 );
	/* Disable the Nagle (TCP No Delay) algorithm */
	flag = 1;
	ret = setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(flag) );
	if (ret == -1) {
	  printf("Couldn't setsockopt(TCP_NODELAY)\n");
	  exit(-1);
	}
#endif
	return 0;
}