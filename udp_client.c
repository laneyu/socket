#include <stdio.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int sockfd;
	struct sockaddr_in server_addr, client_addr;
	const unsigned short server_port = 50000;
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if ( sockfd < 0 )
	{
		printf("socket error in %s...\n", __FUNCTION__);
		return -1;
	}
	
	bzero(&client_addr, sizeof(client_addr));
	client_addr.sin_family = AF_INET;
	client_addr.sin_port   = htons(server_port);
	//client_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	client_addr.sin_addr.s_addr = inet_addr("192.168.18.128");
//	client_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	if ( connect(sockfd, (struct sockaddr *)&client_addr, sizeof(client_addr)))
	{
		printf("connect error in %s...\n", __FUNCTION__);
		return -1;
	}
	
	socklen_t len;
	char msg[256] = "1234";
	int n = sizeof(msg);
	
	printf("1\n");
	sendto(sockfd, msg, n, 0, (struct sockaddr *)&client_addr, sizeof(client_addr));
	printf("2\n");
	n = recvfrom(sockfd, msg, 256, 0, (struct sockaddr *)&server_addr, &len);
	
	printf("msg : %s\n", msg);
	printf("3\n");
	
	return 0;
}
